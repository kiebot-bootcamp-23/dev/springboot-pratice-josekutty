package bootcamp.first.app.jsonDemo.repository;

import bootcamp.first.app.jsonDemo.model.Person;
import jakarta.persistence.NamedQuery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface PersonRepository extends JpaRepository<Person, Integer> {
    List<Person> findByName(String name);
    Optional <Person> findByAge(Integer age);

    @Query(value = "select * from person p where p.name like %:name%", nativeQuery = true)
    List<Person> findSimilarName(String name);



}
