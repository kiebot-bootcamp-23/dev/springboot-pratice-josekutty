package bootcamp.first.app.jsonDemo.controller;

import bootcamp.first.app.jsonDemo.model.Person;
import bootcamp.first.app.jsonDemo.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("api/persons")
public class PersonController {

    @Autowired
    PersonRepository personRepository;

    @RequestMapping()
    List<Person> getAllPersons(@RequestParam(value = "name", required = false) String name, @RequestParam(value = "like", required = false) String like) {
        if(name == null && like==null){
            return personRepository.findAll(Sort.by("name"));
        }
        if(like !=null){
            return personRepository.findSimilarName(like);
        }

        return personRepository.findByName(name);

    }


    @GetMapping(value = "/{id}")
    ResponseEntity getPerson(@PathVariable Integer id) {
        final Optional<Person>  optionalPerson=  personRepository.findById(id);
        if(optionalPerson.isPresent()){
            return ResponseEntity.ok(optionalPerson.get());
        }
        return ResponseEntity.badRequest().body("id not found");
    }
    @PutMapping(value = "/{id}")
    ResponseEntity updatePersons(@PathVariable Integer id, @RequestBody Person person) {
        final Optional<Person>  optionalPerson=  personRepository.findById(id);
        if(optionalPerson.isEmpty()){
            return ResponseEntity.badRequest().body("id not found");
        }
        Person existingPerson = optionalPerson.get();
        existingPerson.setName(person.getName());
        existingPerson.setAge(person.getAge());
        return ResponseEntity.ok(personRepository.save(existingPerson));

    }
    @DeleteMapping(value = "/{id}")
    ResponseEntity deletePerson(@PathVariable Integer id) {
        final Optional<Person>  optionalPerson=  personRepository.findById(id);
        if(optionalPerson.isEmpty()){
            return ResponseEntity.badRequest().body("id not found");
        }
        personRepository.deleteById(id);

        return ResponseEntity.noContent().build();

    }
    @PostMapping("")
    ResponseEntity createPerson( @RequestBody Person person) {

        return ResponseEntity.ok(personRepository.save(person));

    }

}
