package bootcamp.first.app.jsonDemo.controller;

import bootcamp.first.app.jsonDemo.model.Person;
import bootcamp.first.app.jsonDemo.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/test")
public class testController {
    List<Person> persons = List.of(new Person(1, "manu", 23), new Person(1, "manu", 23));
    Map<String, String> map = new HashMap<>();


    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Person> getAllPersons() {
        return persons;
    }

    @RequestMapping(value = "/one", produces = MediaType.APPLICATION_JSON_VALUE)
    Person getOnePerson() {
        return persons.get(0);
    }

    @RequestMapping(value = "/map", produces = MediaType.APPLICATION_JSON_VALUE)
    Map<String, String> getAllCountry() {

        map.put("1", "India");
        map.put("2", "Australia");
        map.put("3", "England");
        map.put("4", "South Africa");
        return map;
    }

    @RequestMapping(value = "/xml", produces = MediaType.APPLICATION_XML_VALUE)
    List<Person> getXml() {
        return persons;
    }
}
